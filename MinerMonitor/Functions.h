#ifndef Functions_h
#define Functions_h
#include <Arduino.h>

void resetFaults(byte *FAULT1, byte *FAULT2);
void scanTempProbes();
float getTempCelsius(byte addr[8], byte type_s);
void loadFAULT(byte *FAULT1, byte *FAULT2);
void saveFAULT(byte *FAULT1, byte *FAULT2);
void relay(byte on);

#endif
