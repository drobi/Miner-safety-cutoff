#include "Functions.h"

//For the temperature probes
#include "OneWire.h"
OneWire  ds(10);  // on pin 10 (a 4.7K resistor is necessary)

// Contains EEPROM.read() and EEPROM.write()
#include <EEPROM.h>

void resetFaults(byte *FAULT1, byte *FAULT2){
  Serial.println("Resetting Faults.");
  *FAULT1 = 0;
  *FAULT2 = 0;
  saveFAULT(FAULT1, FAULT2);
}

void relay(byte on){
  // Relay wired to D8
  // Buzzer wired to D9
  pinMode(8, OUTPUT);
  pinMode(9, OUTPUT);
  Serial.print("relay ");  
  if(on){
    Serial.println("ON"); 
    // TURN ON RELAY
    // TURN OFF BUZZER
    digitalWrite(8, HIGH);
    digitalWrite(9, LOW);
  }else{
    Serial.println("OFF"); 
    // TURN OFF RELAY
    // TURN ON BUZZER
    digitalWrite(8, LOW);
    digitalWrite(9, HIGH);
  }
}

void loadFAULT(byte *FAULT1, byte *FAULT2){
  // check that the 3 memory bytes have the same content, otherwise it means EEPROM is corrupt.
  *FAULT1 = EEPROM.read(0);
  *FAULT2 = EEPROM.read(3);
  if(*FAULT1 != EEPROM.read(1) || *FAULT1 != EEPROM.read(2) || *FAULT2 != EEPROM.read(4) || *FAULT2 != EEPROM.read(5)){
    bitSet(*FAULT1,0); //EEPROM failure  
  }
}

void saveFAULT(byte *FAULT1, byte *FAULT2){
  //save 3 times to check for EEPROM failure.
  EEPROM.update(0, *FAULT1);  
  EEPROM.update(1, *FAULT1);  
  EEPROM.update(2, *FAULT1); 
  EEPROM.update(3, *FAULT2);  
  EEPROM.update(4, *FAULT2); 
  EEPROM.update(5, *FAULT2); 

  //check and return itself (unless EEPROM fault)
  loadFAULT(FAULT1, FAULT2);
}

float getTempCelsius(byte addr[8], byte type_s){
  byte i;
  byte present = 0;
  byte data[12];
  float celsius, fahrenheit;

  ds.reset();
  ds.select(addr);
  ds.write(0x44, 1);        // start conversion, with parasite power on at the end
  
  delay(1000);     // maybe 750ms is enough, maybe not
  // we might do a ds.depower() here, but the reset will take care of it.
  
  present = ds.reset();
  ds.select(addr);    
  ds.write(0xBE);         // Read Scratchpad

  /*Serial.print("  Data = ");
  Serial.print(present, HEX);
  Serial.print(" ");*/
  for ( i = 0; i < 9; i++) {           // we need 9 bytes
    data[i] = ds.read();
    /*Serial.print(data[i], HEX);
    Serial.print(" ");*/
  }
  /*Serial.print(" CRC=");
  Serial.print(OneWire::crc8(data, 8), HEX);
  Serial.println();*/

  // Convert the data to actual temperature
  // because the result is a 16 bit signed integer, it should
  // be stored to an "int16_t" type, which is always 16 bits
  // even when compiled on a 32 bit processor.
  int16_t raw = (data[1] << 8) | data[0];
  if (type_s) {
    raw = raw << 3; // 9 bit resolution default
    if (data[7] == 0x10) {
      // "count remain" gives full 12 bit resolution
      raw = (raw & 0xFFF0) + 12 - data[6];
    }
  } else {
    byte cfg = (data[4] & 0x60);
    // at lower res, the low bits are undefined, so let's zero them
    if (cfg == 0x00) raw = raw & ~7;  // 9 bit resolution, 93.75 ms
    else if (cfg == 0x20) raw = raw & ~3; // 10 bit res, 187.5 ms
    else if (cfg == 0x40) raw = raw & ~1; // 11 bit res, 375 ms
    //// default is 12 bit resolution, 750 ms conversion time
  }
  celsius = (float)raw / 16.0;
  fahrenheit = celsius * 1.8 + 32.0;
  return celsius;
}

void scanTempProbes(){
  byte i;
  byte type_s;
  byte addr[8];
  float celsius, fahrenheit;
  
  Serial.println("Scanning all oneWire devices...");
  
  while ( ds.search(addr)) {
    Serial.print("ROM =");
    for( i = 0; i < 8; i++) {
      Serial.write(' ');
      Serial.print(addr[i], HEX);
    }
  
    if (OneWire::crc8(addr, 7) != addr[7]) {
        Serial.println("CRC is not valid!");
        return;
    }
    Serial.println();
   
    // the first ROM byte indicates which chip
    switch (addr[0]) {
      case 0x10:
        Serial.println("  Chip = DS18S20  Type = 1");  // or old DS1820
        type_s = 1;
        break;
      case 0x28:
        Serial.println("  Chip = DS18B20  Type = 0");
        type_s = 0;
        break;
      case 0x22:
        Serial.println("  Chip = DS1822  Type = 0");
        type_s = 0;
        break;
      default:
        Serial.println("Device is not a DS18x20 family device.");
        return;
    }
  }
  Serial.println("Scan complete.");
  Serial.println();
  ds.reset_search();
  delay(250);
}
