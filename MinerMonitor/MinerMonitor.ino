/* CONCEPT
- The relay controls the power to the miners.
- The relay will only turn on when ALL TESTS PASS.
- The COM port (connected to miner) will be used to monitor status, see faults, and reset faults.
- The COM port will continusously stream status. User can type "r" to reset faults.
- Typ
e "s" to scan all temperature sensors (useful to add new ones in the code).
- To reset faults, the computer needs to bypass the relay (manual procedure done in-person).
- Arduino auto-reset on COM connection was disabled.
- The test is rerun continuously.
- A watchdog timer should automatically reset the Arduino if it freezes.
- When a fault occurs, it is saved to EEPROM to prevent the computer to restart until a physical inspection can be done.
- As part of the test, it needs to check if there was a previous fault recorded in EEPROM.
- The first eeprom address contains the sum of all faults (helps to ensure EEPROM integrity).
*/

/* PINOUT
Reset on COM connect must be disabled
Temperature probes data line: pin10
Pullup resistor (4.7k) is needed on pin10.
Inside smoke detector wired to A0
Outside smoke detector wired to A1
Relay wired to D8
Buzzer wired to D9
*/

#include "Functions.h"

#include <avr/wdt.h>

#define SMOKESENSITIVITY 450 //how much the value can increase from starting value.
#define SMOKEMIN 10 //minimum value the sensor should have.
#define SMOKEMAX 550 //maximum value the sensor should see.
#define TEMPMIN 10
#define TEMPMAX 50

// Fault arrays here
byte FAULT1 = 0; //bits: 7-temp2max 6-smoke1low 5-smoke1high 4-temp1min 3-temp1max 2-smoke2low 1-smoke2high 0-eeprom
byte FAULT2 = 0; //bits: 7-free 6-free 5-free 4-free 3-free 2-smoke2increase 1-smoke1increase 0-temp2min

// Min/Max (not saved in EEPROM)
float minSmoke2, maxSmoke2, minSmoke1, maxSmoke1, minTemp1, maxTemp1, minTemp2, maxTemp2;
byte firstLoop = true;

byte tempAddress1[8] = {0x28, 0xFF, 0x18, 0x55, 0x23, 0x17, 0x03, 0x43};
byte tempAddress2[8] = {0x28, 0x9B, 0xBD, 0xDC, 0x06, 0x00, 0x00, 0x36};

void setup() {
  relay(false); //ensure the relay is off (and turn on buzzer for a short time)
  Serial.begin(115200);  
  delay(3000);
  wdt_enable(WDTO_4S); //auto reset if board freezes
  Serial.println("TYPE 'r' to reset faults and 's' to scan temperature probes (will need firmware update to use them).");
  Serial.println("Initializing...");

  // Load FAULT from EEPROM
  loadFAULT(&FAULT1, &FAULT2);

  Serial.println("Done. Starting loop.");
}

void loop() {
  wdt_reset();
  //delay(1000);
  Serial.println();

  // User inputs
  if(Serial.available() > 0){
    char typed = Serial.read();
    if(typed == 'r'){
      resetFaults(&FAULT1, &FAULT2);
    }
    if(typed == 's'){
      scanTempProbes();
    }
  }

  // get temperatures before printing to screen (looks better since this step takes time)
  float temp1 = getTempCelsius(tempAddress1, 0);
  float temp2 = getTempCelsius(tempAddress2, 0);

  // print programmed limits
  Serial.println("LIMITS");
  Serial.print("Min/max temp: ");
  Serial.print(TEMPMIN);
  Serial.print(" / ");
  Serial.print(TEMPMAX);
  Serial.println(" C");
  Serial.print("Min/max smoke sensor: ");
  Serial.print(SMOKEMIN);
  Serial.print(" / ");
  Serial.print(SMOKEMAX);
  Serial.print(" (max increase: ");
  Serial.print(SMOKESENSITIVITY);
  Serial.println(")");
  Serial.println();
  
  // Check inside smoke detector
  static int A0_init = analogRead(A0); //initial value
  int signal = analogRead(A0);
  Serial.print("Inside smoke detector: ");
  Serial.print(signal);
  Serial.print(" (init: ");
  Serial.print(A0_init);
  Serial.println(")");
  if(signal < SMOKEMIN){
    bitSet(FAULT1, 6);
  }
  if(signal > SMOKEMAX){
    bitSet(FAULT1, 5);
  }
  if(signal - A0_init > SMOKESENSITIVITY){
    bitSet(FAULT2, 1);
  }
  if(firstLoop || signal < minSmoke1) minSmoke1 = signal;
  if(firstLoop || signal > maxSmoke1) maxSmoke1 = signal;
  Serial.print("Min/Max: ");
  Serial.print(minSmoke1);
  Serial.print(", ");
  Serial.println(maxSmoke1);

  // Check room smoke detector
  static int A1_init = analogRead(A1);
  signal = analogRead(A1);
  Serial.print("Room smoke detector: ");
  Serial.print(signal);
  Serial.print(" (init: ");
  Serial.print(A1_init);
  Serial.println(")");
  if(signal < SMOKEMIN){
    bitSet(FAULT1, 2);
  }
  if(signal > SMOKEMAX){
    bitSet(FAULT1, 1);
  }
  if(signal - A1_init > SMOKESENSITIVITY){
    bitSet(FAULT2, 2);
  }
  if(firstLoop || signal < minSmoke2) minSmoke2 = signal;
  if(firstLoop || signal > maxSmoke2) maxSmoke2 = signal;
  Serial.print("Min/Max: ");
  Serial.print(minSmoke2);
  Serial.print(", ");
  Serial.println(maxSmoke2);

  // Check temperatures
  Serial.print("Exhaust temperature: ");
  Serial.print(temp1);
  Serial.println(" C");
  if(temp1 < TEMPMIN){
    bitSet(FAULT1, 4);
  }
  if(temp1 > TEMPMAX){
    bitSet(FAULT1, 3);
  }
  if(firstLoop || temp1 < minTemp1) minTemp1 = temp1;
  if(firstLoop || temp1 > maxTemp1) maxTemp1 = temp1;
  Serial.print("Min/Max: ");
  Serial.print(minTemp1);
  Serial.print(", ");
  Serial.println(maxTemp1);

  Serial.print("Case bottom temperature: ");
  Serial.print(temp2);
  Serial.println(" C");
  if(temp2 < TEMPMIN){
    bitSet(FAULT2, 0);
  }
  if(temp2 > TEMPMAX){
    bitSet(FAULT1, 7);
  }
  if(firstLoop || temp2 < minTemp2) minTemp2 = temp2;
  if(firstLoop || temp2 > maxTemp2) maxTemp2 = temp2;
  Serial.print("Min/Max: ");
  Serial.print(minTemp2);
  Serial.print(", ");
  Serial.println(maxTemp2);

  // Save the status to memory in case of power failure, we want the state to be remembered.
  saveFAULT(&FAULT1, &FAULT2);

  // Check status and set relay
  if(FAULT1 == 0 && FAULT2 == 0){
    Serial.print("All normal - ");
    relay(true);
  }else{
    Serial.print("FAULT1: 0x");
    Serial.println(FAULT1,HEX);
    Serial.print("FAULT2: 0x");
    Serial.println(FAULT2,HEX);
    relay(false);
  }

  // Print faults
  if(bitRead(FAULT1,7)){
    Serial.println("   Case bottom temp high (fan failure?)");  
  }
  if(bitRead(FAULT1,6)){
    Serial.println("   Inside case smoke detector low voltage (defective?)");  
  }
  if(bitRead(FAULT1,5)){
    Serial.println("   Inside case smoke detector high voltage (defective?)");  
  }
  if(bitRead(FAULT1,4)){
    Serial.println("   Exhaust temp low (sensor problem?)");  
  }
  if(bitRead(FAULT1,3)){
    Serial.println("   Exhaust temp high (fan failure?)");  
  }
  if(bitRead(FAULT1,2)){
    Serial.println("   Room smoke detector low voltage (defective?)");  
  }
  if(bitRead(FAULT1,1)){
    Serial.println("   Room smoke detector high voltage (defective?)");  
  }
  if(bitRead(FAULT1,0)){
    Serial.println("   EEPROM ERROR (maintenance needed)"); 
  }
  if(bitRead(FAULT2,0)){
    Serial.println("   Case bottom temp low (sensor problem?)");  
  }
  if(bitRead(FAULT2,1)){
    Serial.println("   Inside smoke detector increased too much (smoke?)");  
  }
  if(bitRead(FAULT2,2)){
    Serial.println("   Room smoke detector increased too much (smoke?)");  
  }
  if(bitRead(FAULT2, 3) || bitRead(FAULT2, 4) || bitRead(FAULT2, 5) || bitRead(FAULT2, 6) || bitRead(FAULT2, 7)){
    Serial.println("   Wrong error code! Firmware problem.");  
  }

  firstLoop = false;
}
